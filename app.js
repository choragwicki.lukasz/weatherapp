const btnViews = document.querySelectorAll('.today, .week');
const boxesTemperature = document.querySelector('.boxes-temperature');
const apiKey = 'w7yHKZcC9aR4rY3cTgbpWP1Li4xHG7l6';
const apiUrl = 'http://dataservice.accuweather.com';
const loader = document.querySelector('.loader');
const loaderImg = document.querySelector('.loader-img');
const city = document.querySelector('.city');
const temperatureNow = document.querySelector('.temperature');
const rainNow = document.querySelector('.rain-information');
const dayOfTheWeek = document.querySelector('.day-of-the-week');
const date = new Date();
const weatherInformation = document.querySelector('.weather-information');
const [celsius, fahrenheit] = document.querySelectorAll('.celsius, .fahrenheit');

let lat;
let lon;
let isFahrenheit = false;

const parsCelsiusToFahrenheit = (value) => Math.round((value * 9) / 5) + 32;

const parsFahrenheitToCelsius = (value) => Math.round((value - 32) * (5 / 9));

const fetchOurLocation = () => {
    toggleLoader(true);
    navigator.geolocation.getCurrentPosition(({ coords: { latitude, longitude } }) => {
        lat = latitude;
        lon = longitude;

        getPositionDetailsByLatLon();
    }, () => {
    }, {
        enableHighAccuracy: true
    });
};

const setTargetLocation = () =>  document.querySelector('.fa-sun-o').addEventListener('click', () => {
    getPositionDetailsByLatLon();
};


const searchActions = () => {
    document.querySelector('.search-location').addEventListener('keyup', async (e) => {
        if (e.keyCode === 13) {
            toggleLoader(true);

            if (e.target.value !== '') {
                const location = (await fetch(`${ apiUrl }/locations/v1/cities/search?apikey=${ apiKey }&q=${ e.target.value }`)
                    .then(response => response.json()))[0];
                location ? getWeather(location.Key, location.LocalizedName) : getPositionDetailsByLatLon();
            } else {
                getPositionDetailsByLatLon();
            }
        }
    });
};

const getPositionDetailsByLatLon = async () => {
    const { LocalizedName, Key } = await fetch(`${ apiUrl }/locations/v1/cities/geoposition/search?apikey=${ apiKey }&q=${ lat },${ lon }`)
        .then(response => response.json());
    getWeather(Key, LocalizedName);
};

const toggleLoader = (isVisible) =>  isVisible ? loader.classList.add('loader--active') : loader.classList.remove('loader--active');

const randomValue = (min, max) => Math.floor(Math.random() * (max - min)) + min;

const randomLoader = () => loaderImg.classList.add(randomValue(1, 3) === 1 ? 'loader-sun' : 'loader-cloudy');

const getWeather = async (key, localizedName) => {
    city.innerHTML = `<h2>${ localizedName }</h2>`;
    toggleLoader(true);

    const [conditionNow, conditionOneDay, conditionFiveDay] = await Promise.all([
        fetch(`${ apiUrl }/currentconditions/v1/${ key }?apikey=${ apiKey }&details=true&metric=true`).then(x => x.json()),
        fetch(`${ apiUrl }/forecasts/v1/daily/1day/${ key }?apikey=${ apiKey }&details=true&metric=true`).then(x => x.json()),
        fetch(`${ apiUrl }/forecasts/v1/daily/5day/${ key }?apikey=${ apiKey }&details=true&metric=true`).then(x =>x.json())
    ]);

    const createFiveDays = () => {
        toggleLoader(true);
        boxesTemperature.innerHTML = '';
        conditionFiveDay.DailyForecasts.forEach(el => boxesTemperature.innerHTML += createWeatherTile(el));
        toggleLoader(false);
    };

    const createWeatherTile = (data) => {
        return `<div class="box-temp">
                    <h2 class="box-day">${ setDayOfTheWeek(data.Date)}</h2>
                    <img class="box-img" src="icons/black/svg/${ getWeatherIcon(data.Day.Icon) }" alt="">
                    <div class="container-temp">
                      <h3 class="box-temp" data-temp>${ Math.round(data.Temperature.Maximum.Value) }°C</h3>
                    <h3 class="box-temp temp-night" data-temp>${ Math.round(data.Temperature.Minimum.Value) }°C</h3>
                   </div>
              </div>`;
    };


    const createWeatherInformation = (conditionNow, conditionOneDay) => {
        toggleLoader(true);

        const setHumidity = () => {
            if (conditionNow[0].IndoorRelativeHumidity < 30) {
                return 'To Dry 👎';
            } else if (conditionNow[0].IndoorRelativeHumidity >= 30 && conditionNow[0].IndoorRelativeHumidity < 61 ) {
                return 'Healthy Zone 👍';
            } else {
                return 'Too Moist 👎';
            }
        };

        weatherInformation.innerHTML = `<h2>Today's Highlights</h2>
                                        <div class="boxes-information">
                                                <div class="boxes-top d-flex justify-between">
                                                    <div class="box-info uv-index d-flex flex-column align-center justify-around">
                                                        <h4 class="title-box">UV Index</h4>
                                                        <h2>${ conditionNow[0].UVIndex }</h2>
                                                         <h5></h5>
                                                    </div>
                                                    <div class="box-info wind-status d-flex flex-column align-center justify-around">
                                                        <h4 class="title-box">Wind Status</h4>
                                                        <h2>${ conditionNow[0].Wind.Speed.Metric.Value } ${ conditionNow[0].Wind.Speed.Metric.Unit }</h2>
                                                        <h3>${ conditionNow[0].Wind.Direction.English }</h3>
                                                    </div>
                                                    <div class="box-info sunrise-sunset d-flex flex-column align-center justify-around">
                                                        <h4 class="title-box">Sunrise & Sunset</h4>
                                                        <div class="sunrise">
                                                            <h2>${ new Date(conditionOneDay.DailyForecasts[0].Sun.Rise).getHours()}:${ new Date(conditionOneDay.DailyForecasts[0].Sun.Rise).getMinutes() }</h2>
                                                        </div>
                                                        <div class="sunset">
                                                            <h2>${ new Date(conditionOneDay.DailyForecasts[0].Moon.Set).getHours()}:${ new Date(conditionOneDay.DailyForecasts[0].Moon.Set).getMinutes() }</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="boxes-bottom d-flex justify-between">
                                                    <div class="box-info humidity d-flex flex-column align-center justify-around">
                                                        <h4 class="title-box">Humidity</h4>
                                                        <h2>${ conditionNow[0].IndoorRelativeHumidity }%</h2>
                                                        <h4>${ setHumidity() } </h4>
                                                    </div>
                                                    <div class="box-info visibility d-flex flex-column align-center justify-around">
                                                        <h4 class="title-box">Visibility</h4>
                                                        <h2>5.2km</h2>
                                                        <h5></h5>
                                                    </div>
                                                    <div class="box-info air-quality d-flex flex-column align-center justify-around">
                                                        <h4 class="title-box">Cloud Cover</h4>
                                                        <h2>${ conditionNow[0].CloudCover }%</h2>
                                                        <h5></h5>
                                                    </div>
                                                </div>
                                            </div>`;
        toggleLoader(false);


    };

    const switchViews = () => {
        btnViews.forEach(el => {
            el.addEventListener('click', () => {
                btnViews.forEach(element => element.classList.remove('view-active'));
                el.classList.add('view-active');
                el.classList.contains('today') ? initTodayView() : initWeekView();
                setTemperature();
            });
        });
    };

    const setTemperature = () => {
        document.querySelectorAll('[data-temp]').forEach(x => {
            if (isFahrenheit && x.textContent.includes('°C')) {
                x.textContent = (parsCelsiusToFahrenheit(Number(x.textContent.replace('°C', ''))) + 'F');
            } else if (!isFahrenheit && !x.textContent.includes('°C')) {
                x.textContent = (parsFahrenheitToCelsius(Number(x.textContent.replace('F', ''))) + '°C');
            }
        });
    };

    const changeTemperatureUnit = () => {
        fahrenheit.addEventListener('click', () => {
            isFahrenheit = true;
            setTemperature();
        });

        celsius.addEventListener('click', () => {
            isFahrenheit = false;
            setTemperature();
        });
    };

    const initTodayView = () => boxesTemperature.innerHTML = createWeatherTile(conditionOneDay.DailyForecasts[0]);

    const initWeekView = () => createFiveDays();

    const initView = () => {
        const fBtn = Array.from(btnViews).find(el => el.classList.contains('view-active'));
        fBtn.classList.contains('today') ? initTodayView() : initWeekView();
    };

    toggleLoader(false);
    setWeatherNow(conditionNow);
    createWeatherTile(conditionOneDay.DailyForecasts[0]);
    initView();
    switchViews();
    createWeatherInformation(conditionNow, conditionOneDay, conditionFiveDay);
    changeTemperatureUnit();
};


const setWeatherNow = (conditionNow) => {
    document.querySelector('.weather-icon').innerHTML = `<img class="aside-photo-weather" src="icons/black/svg/${ getWeatherIcon(conditionNow[0].WeatherIcon) }" alt="">`;
    temperatureNow.textContent = `${ Math.round(conditionNow[0].ApparentTemperature.Metric.Value) }°C`;
    rainNow.innerHTML = `<img src="icons/black/svg/${ getWeatherIcon(conditionNow[0].WeatherIcon) }" alt="">
                         <h2>Rain: ${ conditionNow[0].PrecipitationSummary.Past24Hours.Metric.Value} mm</h2>`;
    dayOfTheWeek.textContent = `${ setDayOfTheWeek(conditionNow[0].LocalObservationDateTime) }, ${ setTimeNow() }`;
};

const setDayOfTheWeek = (date) => {
    const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    return days[new Date(date).getDay()];
};

const setTimeNow = () => {
    const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();

    return `${ hours }:${ minutes }`;
};

const getWeatherIcon = (iconNumber) => {
    const mapIcons = {
        '1': 'sunny.svg',
        '2': 'mostlysunny.svg',
        '3': 'mostlysunny.svg',
        '4': 'mostlysunny.svg',
        '5': 'hazy.svg',
        '6': 'mostlycloudy.svg',
        '7': 'cloudy.svg',
        '8': 'cloudy.svg',
        '9': 'fog.svg',
        '10': 'rain.svg',
        '12': 'rain.svg',
        '13': 'rain.svg',
        '14': 'rain.svg',
        '15': 'rain.svg',
        '16': 'rain.svg',
        '17': 'rain.svg',
        '18': 'rain.svg',
        '19': 'nt_flurries.svg',
        '20': 'nt_chanceflurries.svg',
        '21': 'nt_chanceflurries.svg',
        '22': 'snow.svg',
        '23': 'nt_chancesnow.svg',
        '24': 'snow.svg',
        '25': 'sleet.svg',
        '26': 'rain.svg',
        '29': 'rain.svg',
        '30': 'sunny.svg',
        '31': 'snow.svg',
        '32': 'snow.svg',
        '33': 'nt_clear.svg',
        '34': 'nt_partlycloudy.svg',
        '35': 'nt_partlycloudy.svg',
        '36': 'nt_partlycloudy.svg',
        '37': 'hazy.svg',
        '38': 'nt_mostlysunny.svg',
        '39': 'nt_chanceflurries.svg',
        '40': 'nt_chanceflurries.svg',
        '41': 'tstorms.svg',
        '42': 'tstorms.svg',
        '43': 'snow.svg',
        '44': 'snow.svg',
    };

    return mapIcons.hasOwnProperty(iconNumber.toString()) ? mapIcons[iconNumber.toString()] : 'sunny.svg';
}

const init = () => {
    fetchOurLocation();
    randomLoader();
    searchActions();
    setTargetLocation();
};

init();